# Copyright 2010 Adriaan Leijnse <adriaan@leijnse.net>
# Copyright 2012, 2014 Ali Polatel <alip@exherbo.org>
# Copyright 2017 Graham Northup <grissess@nexusg.org>
# Distributed under the terms of the GNU General Public License v2

require non
require toolchain-funcs

SUMMARY="Powerful real-time, pattern-based MIDI sequencer"
DESCRIPTION="
The Non Sequencer is a powerful real-time, pattern-based MIDI sequencer for
Linux--released under the GPL. Filling the void left by countless DAWs,
piano-roll editors, and other purely performance based solutions, it is a
compositional tool--one that transforms MIDI music-making on Linux from a
complex nightmare into a pleasurable, efficient, and streamlined process.
"

PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-cpp/libsigc++
"

BUGS_TO="alip@exherbo.org"

src_configure() {
	# Temporarily fixes an upstream bug where the sources assume GCC always
	# does C++11
	if cc-is-gcc && [[ "$(gcc-major-version)" < 6 ]]; then
		# FIXME: flag-o-matic doesn't yet support `-std=xxx` in CXXFLAGS
		export CXXFLAGS="${CXXFLAGS} -std=c++11"
	fi

	waf_src_configure
}
