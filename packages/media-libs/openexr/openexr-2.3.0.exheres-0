# Copyright 2008 Thomas Anderson
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'openexr-1.6.1.ebuild' which is:
#   Copyright 1999-2008 Gentoo Foundation

require github [ release=v${PV} suffix=tar.gz ] \
    toolchain-funcs \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="High dynamic-range image file format library"
HOMEPAGE+=" https://www.${PN}.com/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="examples"

# Some example files are missing from the tarball (e.g. comp_dwaa_v1.exr)
# https://lists.gnu.org/archive/html/openexr-devel/2014-08/msg00022.html
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/ilmbase[>=${PV}]
        sys-libs/zlib
"

AT_M4DIR=( m4 )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2.3.0-Fix-build-when-updating-from-an-older-version.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # TODO: perhaps enable via expensive_tests
    --disable-imfhugetest
    --disable-imffuzztest
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'examples imfexamples'
)

DEFAULT_SRC_INSTALL_PARAMS=(
    docdir=/usr/share/doc/${PNVR}
    examplesdir=/usr/share/doc/${PNVR}/examples
)

openexr-cc_define_enable() {
    cc-has-defined ${1} && echo --enable-${2} || echo --disable-${2}
}

src_prepare() {
    autotools_src_prepare

    edo sed -e 's:"/var/tmp/":"'"${TEMP}"'":' -i IlmImfTest/tmpDir.h
}

src_configure() {
    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
            option_enable ${s} ; \
        done )
    )

    myconf+=(
        $(openexr-cc_define_enable __AVX__ avx)
    )

    econf "${myconf[@]}"
}

src_install() {
    default

    if option examples; then
        dobin IlmImfExamples/imfexamples
    else
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/examples
    fi
}

