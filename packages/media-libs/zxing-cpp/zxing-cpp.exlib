# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user="nu-book" tag=v${PV} ] cmake [ api=2 ]

export_exlib_phases src_install

SUMMARY="C++ port of the ZXing barcode scanning library"
DESCRIPTION="
- In pure C++11, no third-party dependencies
- Stateless, thread-safe readers/generators

Same as with ZXing, the  following barcode are supported:
1D product | 1D industrial | 2D
---------------------------------------------------
UPC-A      | Code 39       | QR Code
UPC-E      | Code 93       | Data Matrix
EAN-8      | Code 128      | Aztec (beta)
EAN-13     | Codabar       | PDF 417 (beta)
           | ITF
           | RSS-14
           | RSS-Expanded
"

LICENCES="
    Apache-2.0
    LGPL-2.1 [[ note = [ Part imported from Qt ] ]]
"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

BUGS_TO="heirecka@exhberbo.org"

# FIXME: Tests live in dir outside of the other sources and want to download
#        gtest.
RESTRICT="test"

CMAKE_SOURCE="${WORKBASE}/${PNV}/core"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS=TRUE
    -DENABLE_ENCODERS:BOOL=TRUE
    -DENABLE_DECODERS:BOOL=TRUE
)

zxing-cpp_src_install() {
    cmake_src_install

    edo pushd "${WORKBASE}/${PNV}"
    emagicdocs
    edo popd
}

