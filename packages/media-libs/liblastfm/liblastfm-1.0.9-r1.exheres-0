# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=lastfm tag=${PV} ] cmake [ api=2 ]

SUMMARY="Collection of libraries to help integrate Last.fm services"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( qt4 qt5 ) [[ number-selected = at-least-one ]]
"

# tests want to connect to Last.fm
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libsamplerate
        sci-libs/fftw[>=3.0]
        qt4? ( x11-libs/qt:4[>=4.4.0][dbus][sql] )
        qt5? ( x11-libs/qtbase:5[sql] )
    run:
        qt4? ( x11-libs/qt:4[sqlite] )
        qt5? ( x11-libs/qtbase:5[sqlite] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/14ad8beffe6ac88ee9d22c4036b752c680773f69.patch
    "${FILES}"/480e2ec663ef867e7892dbbc624737cae176c717.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_FINGERPRINT:BOOL=TRUE
    -DBUILD_DEMOS:BOOL=FALSE
    -DBUILD_TESTS:BOOL=FALSE
)

src_configure() {
    if option qt4; then
        edo mkdir "${WORKBASE}"/qt4-build
        edo pushd "${WORKBASE}"/qt4-build
        ecmake -DBUILD_WITH_QT4:BOOL=TRUE "${CMAKE_SRC_CONFIGURE_PARAMS[@]}"
        popd
    fi
    if option qt5; then
        edo mkdir "${WORKBASE}"/qt5-build
        edo pushd "${WORKBASE}"/qt5-build
        ecmake -DBUILD_WITH_QT4:BOOL=FALSE "${CMAKE_SRC_CONFIGURE_PARAMS[@]}"
        popd
    fi
}

src_compile() {
    if option qt4; then
        edo pushd "${WORKBASE}"/qt4-build
        emake
        edo popd
    fi
    if option qt5; then
        edo pushd "${WORKBASE}/"qt5-build
        emake
        edo popd
    fi
}

src_install() {
    if option qt4; then
        edo pushd "${WORKBASE}"/qt4-build
        emake DESTDIR="${IMAGE}" install
        edo popd
    fi
    if option qt5; then
        edo pushd "${WORKBASE}"/qt5-build
        emake DESTDIR="${IMAGE}" install
        edo popd
    fi

    edo pushd "${CMAKE_SOURCE}"
    emagicdocs
    edo popd
}

