# Copyright 2010 Yury G. Kudryashov <urkud@ya.ru>
# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'opencv-2.0.0-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

MY_PN=OpenCV

require cmake [ api=2 ] flag-o-matic github
require python [ has_bin=false has_lib=false blacklist="none" min_versions="2.6 3.2" ]

SUMMARY="A collection of algorithms and sample code for various computer vision problems"
HOMEPAGE="http://${PN}.org"
# 460 MiB
#DOWNLOADS+=" https://github.com/Itseez/${PN}_extra/archive/${PV}.tar.gz -> ${PN}_extra-${PV}"
DOWNLOADS+="
    contrib? ( https://github.com/${PN}/${PN}_contrib/archive/${PV}.tar.gz -> ${PN}_contrib-${PV}.tar.gz )"

REMOTE_IDS="sourceforge:${PN}library"

LICENCES="
    BSD-3
    xine? ( GPL-2 )
"
SLOT="0"
PLATFORMS="~amd64 ~x86"

# All listed in modules/contrib_world/CMakeLists.txt minus xfeatures2d, which
# contains no-free parts.
contrib_modules="bgsegm bioinspired ccalib cvv face line_descriptor optflow
    reg rgbd saliency surface_matching text tracking ximgproc xobjdetect
    xphoto
"
MYOPTIONS="doc examples ffmpeg gstreamer gtk ieee1394 java jpeg jpeg2000
    openexr opengl png python qt4 qt5 threads tiff v4l xine

    camera  [[ description = [ Capture using your camera device via libgphoto2 ] ]]
    contrib [[ description = [ Extra modules, possibly without stable API and not well-tested ] ]]
    opencl  [[ description = [ Hardware acceleration via OpenCL ] ]]
    openmp  [[ description = [ Support for Open Multi-Processin ] ]]
    qt5     [[ description = [ Adds support for the Qt GUI/Application Toolkit version 5.x ] ]]
    vtk     [[ description = [ Build Viz module allowing 3D visualization via VTK ] ]]
    webp    [[ description = [ Support for the WebP image format ] ]]

    ( contrib_modules: ( ${contrib_modules} ) [[ *requires = contrib ]] )
    contrib? ( contrib_modules: ( ${contrib_modules} )
        [[ number-selected = at-least-one ]]
    )

    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
    opengl? ( ( gtk qt4 qt5 ) [[ number-selected = at-least-one ]] )
    ( qt4 qt5 ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        sci-libs/eigen:3 [[ note = [ Optional, enable since this is a build-time header-only dependency. ] ]]
        doc? (
            dev-python/Sphinx
            dev-texlive/texlive-latex
        )
        gtk? ( virtual/pkg-config )

    build+run:
        camera?  ( media-libs/libgphoto2 )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        gtk? (
            dev-libs/glib:2   [[ note = gthread ]]
            x11-libs/gtk+:3
        )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394[>=1.2.0]
        )
        java? (
            dev-java/apache-ant
            virtual/jdk:=[>=1.6]   [[ note = [ recommended version spec ] ]]
        )
        jpeg2000? ( media-libs/jasper   )
        opencl?   ( dev-libs/opencl-headers )
        openexr?  (
            media-libs/ilmbase
            media-libs/openexr
        )
        opengl?   (
            x11-dri/glu
            x11-dri/mesa
            gtk? ( x11-libs/gtkglext )
        )
        openmp?   ( sys-libs/libgomp:* )
        png?      ( media-libs/libpng:= )
        python?   ( dev-python/numpy )
        qt4?      ( x11-libs/qt:4[opengl] )
        qt5?      ( x11-libs/qtbase:5 )
        threads?  ( dev-libs/tbb )
        tiff?     ( media-libs/tiff     )
        v4l?      ( media-libs/v4l-utils )
        vtk?      ( sci-libs/vtk[>=5.8.0][qt4=][qt5=] )
        webp?     ( media-libs/libwebp:= )
        xine?     ( media-libs/xine-lib )
        contrib_modules:text? ( app-text/tesseract )
        providers:ffmpeg? ( media/ffmpeg )
        providers:ijg-jpeg? ( media-libs/jpeg )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav )
"

# FIXME: Download this data if tests are enabled and run tests
RESTRICT=test

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PNV}-Allow-disabling-java-bindings.patch
    "${FILES}"/${PNV}-multiarch.patch
    "${FILES}"/3bf16da92d1cb42d429e9a81a3a0abd5cf3360a7.patch
)

pkg_setup() {
    # Force -DNDEBUG because upstream has errors in their debug code
    append-flags -DNDEBUG
}

src_configure() {
    # FIXME: ipp support? (library is non-free / costs money)
    # FIXME: unicap support
    local p cmakeparams=(
        -DPKG_CONFIG_EXECUTABLE:PATH=${PKG_CONFIG}
        -DCMAKE_SKIP_RPATH:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DENABLE_PRECOMPILED_HEADERS:BOOL=FALSE
        -DENABLE_OMIT_FRAME_POINTER:BOOL=FALSE
        -DENABLE_PROFILING:BOOL=FALSE
        -DOPENCV_DOC_INSTALL_PATH:PATH=/usr/share/doc/${PNVR}
        -DOPENCV_SHARE_INSTALL_PATH:PATH=/usr/share
        -DENABLE_DYNAMIC_CUDA:BOOL=FALSE
        -DWITH_CUDA:BOOL=FALSE
        -DWITH_GIGEAPI:BOOL=FALSE
        -DWITH_INTELPERC:BOOL=FALSE
        -DWITH_IPP:BOOL=FALSE
        -DWITH_PVAPI:BOOL=FALSE
        -DWITH_UNICAP:BOOL=FALSE
        -WITH_GTK_2_X:BOOL=FALSE

        $(cmake_build doc DOCS)
        $(cmake_build EXAMPLES)
        $(cmake_option examples INSTALL_C_EXAMPLES)
        $(cmake_build python NEW_PYTHON_SUPPORT)

        $(cmake_with camera GPHOTO2)
        $(cmake_with FFMPEG)
        $(cmake_with GSTREAMER)
        $(cmake_with GTK)
        $(cmake_with ieee1394 1394)
        $(cmake_with JAVA)
        $(cmake_with JPEG)
        $(cmake_with jpeg2000 JASPER)
        $(cmake_with PNG)
        $(cmake_with OPENCL)
        $(cmake_with OPENEXR)
        $(cmake_with OPENGL)
        $(cmake_with OPENMP)
        $(cmake_with threads TBB)
        $(cmake_with TIFF)
        $(cmake_with V4L)
        $(cmake_with VTK)
        $(cmake_with WEBP)
        $(cmake_with XINE)
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE)
    )

    if option qt4 ; then
        cmakeparams+=( -DWITH_QT=4 )
    elif option qt5 ; then
        cmakeparams+=( -DWITH_QT=5 )
    else
        cmakeparams+=( -DWITH_QT=FALSE )
    fi

    if option contrib ; then
        cmakeparams+=(
            -DOPENCV_EXTRA_MODULES_PATH="${WORKBASE}"/opencv_contrib-${PV}/modules
            -DBUILD_opencv_aruco:BOOL=FALSE
            -DBUILD_opencv_datasets:BOOL=FALSE
            -DBUILD_opencv_dnn:BOOL=FALSE
            -DBUILD_opencv_dpm:BOOL=FALSE
            -DBUILD_opencv_fuzzy:BOOL=FALSE
            -DBUILD_opencv_stereo:BOOL=FALSE
            -DBUILD_opencv_structured_light:BOOL=FALSE
            -DBUILD_opencv_xfeatures2d:BOOL=FALSE
        )
        local module
        for module in ${contrib_modules} ; do
            cmakeparams+=(
                $(cmake_build contrib_modules:${module} opencv_${module})
            )
        done
    fi

    ecmake "${cmakeparams[@]}"
}

src_test() {
    # see notice about tests above
    for unittest in $(find bin/ -type f -name "opencv_test_*") ; do
        edo ./${unittest}
    done
}

src_install() {
    cmake_src_install

    # Add a hack until we figure out how to configure this properly via CMake.
    if [[ -d ${IMAGE}/usr/lib/python$(python_get_abi) ]]; then
        edo mv "${IMAGE}"/usr/{lib,${LIBDIR}}/python$(python_get_abi)
        edo find "${IMAGE}"/usr/lib -type d -empty -delete
        python_bytecompile
    fi
}

