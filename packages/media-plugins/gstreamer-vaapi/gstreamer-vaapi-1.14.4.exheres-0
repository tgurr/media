# Copyright 2010 Paul Seidler <sepek@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Switch to meson once gstreamer's meson build system works properly
#require meson

SUMMARY="Decode JPEG, MPEG-2, MPEG-4, H.264, VP8, VP9, VC-1, HEVC with gstreamer via VA-API"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    opengl [[
        description = [ Build OpenGL video output ]
        requires = [ X ]
    ]]
    wayland
    X

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/libtool[>=2.2]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        dev-libs/glib:2[>=2.40]
        media-libs/gstreamer:1.0[>=${PV}]
        media-plugins/gst-plugins-base:1.0[>=${PV}]
        media-plugins/gst-plugins-bad:1.0[>=${PV}]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/libva[>=1.6.0][X?][wayland(-)?]
        X? (
            x11-libs/libX11
            x11-libs/libxkbcommon
            x11-libs/libXrandr
            x11-libs/libXrender
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        wayland? ( sys-libs/wayland[>=1.0.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-drm
    --enable-encoders
    --disable-static
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gtk-doc'
    'opengl egl'
    'opengl glx'
    'wayland'
    'X x11'
)

