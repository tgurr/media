# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gst-plugins-base flag-o-matic

PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc gobject-introspection
    gstreamer_plugins:
        alsa      [[ description = [ Audio input, output and mixing with ALSA ] ]]
        cdda      [[ description = [ Robust CD audio extraction using cdparanoia ] ]]
        libvisual [[ description = [ Audio visualization using libvisual ] ]]
        ogg       [[ description = [ Ogg multimedia container format support ] ]]
        pango     [[ description = [ Pango-based text rendering and video overlaying (needed to display subtitles) ] ]]
        theora    [[ description = [ Theora video encoding and decoding using libtheora ] ]]
        vorbis    [[ description = [ Vorbis audio support using libvorbis ] ]]
        xv        [[ description = [ Video output using the XVideo extension ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        doc? ( dev-doc/gtk-doc[>=1.3] )
        virtual/pkg-config[>=0.20]
    build+run:
        media-libs/gstreamer:0.10[>=0.10.36][gobject-introspection?]
        dev-libs/libxml2[>=2.6]
        dev-libs/glib:2[>=2.24]
        dev-libs/orc:0.4[>=0.4.11]
        media-libs/freetype:2[>=2.0.9]
        app-text/iso-codes
        sys-libs/zlib
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
        gstreamer_plugins:alsa? ( sys-sound/alsa-lib[>=0.9.1] )
        gstreamer_plugins:cdda? ( media/cdparanoia[>=0.10.2] )
        gstreamer_plugins:libvisual? ( media-libs/libvisual )
        gstreamer_plugins:ogg? ( media-libs/libogg[>=1.0] )
        gstreamer_plugins:pango? ( x11-libs/pango[>=1.16] )
        gstreamer_plugins:theora? ( media-libs/libtheora[>=1.1] )
        gstreamer_plugins:vorbis? ( media-libs/libvorbis[>=1.0] )
        gstreamer_plugins:xv? ( x11-libs/libSM
                                x11-libs/libXv )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    test:
        x11-libs/gtk+:3[>=3.0.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-experimental'

    # core plugins
    '--enable-adder'
    '--enable-app'
    '--enable-audioconvert'
    '--enable-audiorate'
    '--enable-audiotestsrc'
    '--enable-encoding'
    '--enable-ffmpegcolorspace'
    '--enable-gdp'
    '--enable-playback'
    '--enable-audioresample'
    '--enable-subparse'
    '--enable-tcp'
    '--enable-typefind'
    '--enable-videotestsrc'
    '--enable-videorate'
    '--enable-videoscale'
    '--enable-volume'
    '--enable-iso-codes'

    # don't compile not installed examples
    '--disable-examples'

    # deprecated vfs
    '--disable-gnome_vfs'

    # already have the dependencies
    '--enable-gio'
    '--enable-gst_v4l'
    '--enable-orc'
    '--with-gudev'
    '--enable-zlib'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'doc gtk-doc'
    'gobject-introspection introspection'

    # optional plugins
    'gstreamer_plugins:alsa'
    'gstreamer_plugins:cdda cdparanoia'
    'gstreamer_plugins:libvisual'
    'gstreamer_plugins:pango'
    'gstreamer_plugins:theora'
    'gstreamer_plugins:xv xvideo'
    'gstreamer_plugins:xv xshm'
    'gstreamer_plugins:ogg ogg'
    'gstreamer_plugins:vorbis vorbis'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'gstreamer_plugins:xv x'
)

src_configure() {
    # This is needed for when the "doc" option is enabled; without it it doesn't use -m32 when
    # building the libraries for documentation.
    filter-flags -m32

    default
}

src_test() {
    unset DBUS_SESSION_BUS_ADDRESS
    unset DISPLAY
    default
}


